#!/usr/bin/env bash

echo "################################## worker.sh"
echo "################################## waiting  for db"
./wait-for-it.sh db:3306
echo "################################## db is ready"

echo "################################## waiting  for db"
./wait-for-it.sh elasticsearch:9200
echo "################################## elasticsearch is ready"


echo "################################## Run Worker"
if [ "$DEV_ENV" == "True" ]; then
    celery -A  config worker -l INFO -n captcha_solver_worker.%h --autoreload
else
    celery -A  config worker -l INFO -n captcha_solver_worker.%h
fi
