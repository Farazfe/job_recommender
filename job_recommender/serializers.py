from rest_framework import serializers
from .models import Position
from job_recommender.utility.exceptions import InvalidExpirationDate
from django.utils import timezone


class PositionSerailizer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = '__all__'

    def create(self, validated_data):
        """
        Check that the expired_at date is after the created_at.
        """
        created_at = timezone.now().date()
        if not validated_data['expired_at'] > created_at:
            raise serializers.ValidationError({"position": InvalidExpirationDate.message})
        position = Position.objects.create(created_at=created_at, **validated_data)
        return position
