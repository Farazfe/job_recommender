from django.core.management.base import BaseCommand
from job_recommender.models import Position
import random
from django.utils import timezone

categories = {

    'IT': ['Python Developer', 'Go Developer', 'Frontend Developer', 'Network Engineer', 'UI Designer', 'UX Designer',
           'DevOps Engineer'],

    'Healthcare': ['Nurse', 'Physical Therapist', 'Pharmacy Assistant', 'Personal Trainer'],
    'Artistic': ['Graphic Designer', 'Interior Designer', 'Photographer'],
    'Finance': ['Auditor', 'Accountant', 'Payroll Manager', 'Financial Analyst']

}

min_age_list = list(range(18, 21)) + [None]
max_age_list = list(range(40, 70, 5)) + [None]

education_list = ['BSc', 'MD', 'PhD', 'diploma']
salary_list = list(range(5, 25, 3))
location_list = ['Tehran', 'Isfahan', 'Mashhad', 'Yazd', 'Gorgan', 'Shiraz']
gender_list = ['male', 'female', 'other', None]


class Command(BaseCommand):
    help = 'Create seed data for Position model'

    def add_arguments(self, parser):
        parser.add_argument('num', type=int, help='Indicates the number of records to be created')

    def handle(self, *args, **kwargs):
        num = kwargs['num']
        for i in range(1, num + 1):
            category = random.choice(list(categories.keys()))
            title = random.choice(categories[category])
            min_age = random.choice(min_age_list)
            max_age = random.choice(max_age_list)
            education = random.choice(education_list)
            salary = random.choice(salary_list)
            location = random.choice(location_list)
            created_at = timezone.now().date()
            expired_at = timezone.timedelta(days=random.randint(1, 365)) + created_at
            gender = random.choice(gender_list)
            obj = Position.objects.create(category=category, title=title, min_age=min_age, max_age=max_age,
                                          education=education, salary=salary, location=location, created_at=created_at,
                                          expired_at=expired_at, gender=gender)
            print(i)
