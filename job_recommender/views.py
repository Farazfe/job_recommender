from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework import serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework import status, generics
from .models import Position
from .serializers import PositionSerailizer

from job_recommender.utility.exceptions import AppException


# Create your views here.


class PositionListView(generics.ListCreateAPIView):
    """
    A view for listing a queryset or creating a Position instance.

    Example:
        # List of all the Position records
        curl -X GET https://server/api/position/

        # Create new Position record
        curl -X POST  https://server/api/position/
            -F 'title=Developer'
            -F 'category=IT'
            -F 'education=BSc'
            -F 'location=Tehran'
            -F 'gender=male'
            -F 'expired_at=2022-05-10'

        available choices for gender: ['male', 'female', 'other']

    :raise
        {"gender":["\"X\" is not a valid choice."]}
    """
    queryset = Position.objects.all()
    serializer_class = PositionSerailizer


class PositionDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    A view for retrieving, updating or deleting a Position instance.

    Example

        # Get instance detail
        curl -X GET https://server/api/position/<id>/

        # Update position record
        curl -X PATCH  https://server/api/position/<id>/
            -F 'salary=20000000'

        # Delete position record
        curl -X DELETE  https://server/api/position/<id>/


    :raise
        {"detail":"Not found."}

    """

    queryset = Position.objects.all()
    serializer_class = PositionSerailizer
    lookup_field = 'pk'
