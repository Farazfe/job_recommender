from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _


class AppException(Exception):
    """
    Base class for App framework exceptions.
    Subclasses should provide  `.message` properties.
    """
    message = _('An application error occurred.')

    def __init__(self, *args, **kwargs):
        self.message = force_text(self.message).format(*args, **kwargs)

    def __str__(self):
        return self.message


class InvalidExpirationDate(AppException):
    message = _('Invalid Expiration date.')
