from django.db import models

GENDER_CHOICE_MALE = 'male'
GENDER_CHOICE_FEMALE = 'female'
GENDER_CHOICE_OTHER = 'other'
GENDER_CHOICES = ((GENDER_CHOICE_MALE, 'Male'),
                  (GENDER_CHOICE_FEMALE, 'Female'),
                  (GENDER_CHOICE_OTHER, 'Other'),
                  )


class Candidate(models.Model):
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    age = models.IntegerField(null=True, blank=True)
    education = models.CharField(max_length=30)
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES, blank=True, null=True)
    expected_salary = models.IntegerField(null=True, blank=True)
    location = models.CharField(max_length=100)


class Position(models.Model):
    title = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    min_age = models.IntegerField(null=True, blank=True)
    max_age = models.IntegerField(null=True, blank=True)
    education = models.CharField(max_length=30)
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES, blank=True, null=True)
    salary = models.IntegerField(null=True, blank=True)
    location = models.CharField(max_length=100)
    created_at = models.DateField(blank=True)
    expired_at = models.DateField()
    lived_at = models.DateField(null=True, blank=True)  # No idea what this field does

    class Meta():
        ordering = ["id"]

    def __str__(self):
        return self.title
