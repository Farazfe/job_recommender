from django.urls import path
from .views import PositionListView, PositionDetail

urlpatterns = [
    path('position/<int:pk>/', PositionDetail.as_view(), name='position-detail'),
    path('position/', PositionListView.as_view(), name='position-list'),

]
