from django.apps import AppConfig


class JobRecommenderConfig(AppConfig):
    name = 'job_recommender'
