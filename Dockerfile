# pull official base image
FROM ubuntu:18.04
RUN apt-get update && apt-get -y install mercurial git python3-pip libjpeg-dev libxml2 gettext libxml2-dev libxslt1-dev gcc python3-dev build-essential libmysqlclient-dev default-libmysqlclient-dev  && pip3 install --upgrade pip

# set work directory
#WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /prj
ADD . /prj
WORKDIR /prj
RUN ./install.sh
COPY entrypoint.sh /
RUN useradd devops
RUN chown -R devops:devops /prj
USER devops
ENTRYPOINT ["/entrypoint.sh"]
