from django_elasticsearch_dsl import Document, fields, Index
from job_recommender.models import Position
from elasticsearch_dsl import analyzer

job_index = Index('jobs')
job_index.settings(
    number_of_shards=1,
    number_of_replicas=0
)
html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter=["lowercase"],
    char_filter=["html_strip"]
)


@job_index.doc_type
class JobDocument(Document):
    """Position Elasticsearch document."""

    id = fields.IntegerField(attr='id')

    category = fields.TextField(
        analyzer=html_strip,
        fields={
            'raw': fields.TextField(analyzer='keyword'),
        }
    )

    location = fields.TextField(
        analyzer=html_strip,
        fields={
            'raw': fields.TextField(analyzer='keyword'),
        }
    )

    education = fields.TextField(
        analyzer=html_strip,
        fields={
            'raw': fields.TextField(analyzer='keyword'),
        }
    )

    title = fields.TextField(
        analyzer=html_strip,
        fields={
            'raw': fields.TextField(analyzer='keyword'),
        }
    )

    gender = fields.TextField(
        analyzer=html_strip,
        fields={
            'raw': fields.TextField(analyzer='keyword'),
        }
    )

    min_age = fields.IntegerField()
    max_age = fields.IntegerField()
    created_at = fields.DateField()
    expired_at = fields.DateField()

    salary = fields.IntegerField()

    class Django:
        model = Position
