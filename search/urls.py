from django.urls import path
from django.conf.urls import include
from .views import JobDocumentView
from rest_framework_extensions.routers import ExtendedDefaultRouter


router = ExtendedDefaultRouter()
jobs = router.register(r'jobs',
                        JobDocumentView,
                        basename='job_document')
urlpatterns = [
    path('', include(router.urls)),

]
