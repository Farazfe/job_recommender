import json

from rest_framework import serializers
from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

from .documents import JobDocument


class JobDocumentSerializer(DocumentSerializer):
    """Serializer for the Job document."""
    score = serializers.SerializerMethodField()

    def get_score(self, obj):
        if hasattr(obj.meta, 'score'):
            return obj.meta.score
        return None

    class Meta:
        # Specify the correspondent document class

        document = JobDocument
        fields = (
            'id', 'title', 'category', 'min_age', 'max_age', 'education', 'gender', 'salary', 'location', 'created_at',
            'expired_at', '_score')
