from django_elasticsearch_dsl_drf.constants import (
    LOOKUP_FILTER_GEO_DISTANCE,
)
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    OrderingFilterBackend,
    CompoundSearchFilterBackend,
    DefaultOrderingFilterBackend
)

from django_elasticsearch_dsl_drf.constants import (
    LOOKUP_FILTER_WILDCARD,
    LOOKUP_QUERY_ISNULL,
    LOOKUP_QUERY_CONTAINS,
    LOOKUP_QUERY_GT,
    LOOKUP_QUERY_LT,
    LOOKUP_FILTER_TERM,
    LOOKUP_FILTER_TERMS,

)

from django_elasticsearch_dsl_drf.viewsets import DocumentViewSet

# Example app models
from search.documents import JobDocument
from search.serializers import JobDocumentSerializer


class JobDocumentView(DocumentViewSet):
    """The JobDocument view."""

    document = JobDocument
    serializer_class = JobDocumentSerializer
    lookup_field = 'pk'
    filter_backends = [
        FilteringFilterBackend,
        OrderingFilterBackend,
        DefaultOrderingFilterBackend,
        CompoundSearchFilterBackend,
    ]
    # Define search fields
    search_fields = (
        'category',
        'title'
        'location',
        'education',
        'gender',
    )
    # Define filtering fields
    filter_fields = {
        # 'id': None,
        'category': {
            'field': 'category.raw',
            'lookups': [
                LOOKUP_QUERY_ISNULL,
                LOOKUP_QUERY_CONTAINS,
                LOOKUP_FILTER_TERM,
                LOOKUP_FILTER_TERMS,
            ],
        },
        'education': {
            'field': 'education.raw',
            'lookups': [
                LOOKUP_QUERY_ISNULL,
                LOOKUP_QUERY_CONTAINS,
                LOOKUP_FILTER_TERM,
                LOOKUP_FILTER_TERMS,
            ],
        },
        'title': {
            'field': 'title.raw',
            'lookups': [
                LOOKUP_QUERY_CONTAINS,
                LOOKUP_FILTER_TERM,
                LOOKUP_FILTER_TERMS,
            ],
        },
        'created_at':
            {
                'field': 'created_at',
                'lookups': [
                    LOOKUP_QUERY_GT,
                    LOOKUP_QUERY_LT
                ]
            },
        'expired_at':
            {
                'field': 'expired_at',
                'lookups': [
                    LOOKUP_QUERY_GT,
                    LOOKUP_QUERY_LT
                ]
            },
        'gender':
            {
                'field': 'gender.raw',
                'lookups': [
                    LOOKUP_QUERY_ISNULL,
                    LOOKUP_FILTER_TERM,
                    LOOKUP_FILTER_TERMS,
                ]
            }
    }
    # Define ordering fields
    ordering_fields = {
        'id': 'id',
        'salary': 'salary',
    }
    # Specify default ordering
    ordering = ('-salary', '_score')
