#!/usr/bin/env bash

echo "################################## web.sh"
echo "################################## waiting  for db"
./wait-for-it.sh db:3306
echo "################################## db is ready"

echo "################################## waiting  for elasticsearch"
./wait-for-it.sh elasticsearch:9200
echo "################################## elasticsearch is ready"

echo "################################## Migrating ..."
python3 manage.py migrate
echo "################################## Migrating finished"

echo "################################## Run server"
if [ "$DEV_ENV" == "True" ]; then
    gunicorn -b 0.0.0.0:8000 -w ${GUNICORN_WORKERS:-3} config.wsgi:application --reload
else
    gunicorn -b 0.0.0.0:8000 -w ${GUNICORN_WORKERS:-3} config.wsgi:application
fi
