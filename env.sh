# Just for the purpose of demonstration!
# Never commit the actual values on the production server!



export DB_NAME=jobs
export DB_USER=root
export DB_PASSWORD=admin
export DB_HOST=db
export DB_PORT=3306
export ELASTICSEARCH_HOST=elasticsearch:9200
export GUNICORN_WORKERS=2
export ALLOWED_HOSTS='*'
export MYSQL_PASSWORD=admin
export MYSQL_ROOT_PASSWORD=admin