# Job Recommender
Job recommender is a matching system for recommending the most relevant job positions to the candidates via Elasticsearch.

## Installation

Use docker-compose inside the root of the project.
Remember to load the proper env variables.
```bash
$ source env.sh
$ docker-compose -f docker-compose_stateful.yml -p dev build
$ docker-compose -f docker-compose_stateful.yml -p dev up -d
```

## Manual Installation
For manual installation, first download the [Elasticsearch](https://www.elastic.co/downloads/elasticsearch) and [MySQL](https://www.mysql.com/downloads/) packages.

Create the database:
```
$ mysql -u root -p
CREATE DATABASE jobs;
CREATE USER 'mysql'@'%' IDENTIFIED WITH mysql_native_password BY 'admin';
GRANT ALL ON recommender.* TO 'mysql'@'%';
FLUSH PRIVILEGES;
```
Install the dependencies:
```
pip3 install -r requirements.txt
```

Then run the server inside the root of the project:
```
python manage.py runserver #Development server
```
OR

```
gunicorn -b 0.0.0.0:8000 -w ${GUNICORN_WORKERS:-3} config.wsgi:application #Production server

```
## CRUD Operations
CRUD operations on Position model:

```bash
curl -X POST http://server/api/position/ -F 'title=Python Developer'
 -F 'category=IT' -F 'education=BSc' -F 'location=Tehran'
 -F 'gender=female' -F 'expired_at=2052-05-10'     #Create

curl -X GET https://server/api/position/ #List all the positions

curl -X GET https://server/api/position/<id>/ #Retrieve

curl -X PATCH  https://server/api/position/<id>/
            -F 'salary=20000000'            #Update

curl -X DELETE  https://server/api/position/<id>/  #Delete
```
After each CRUD operation, Elasticsearch **will be updated automatically** through Django signals.
So primary DB and Elasticsearch would be always in sync.
If you have pre-existing data in your primary DB, use:
```
python manage.py search_index --rebuild

```
To re-create the index in ES.


## Seeding Data
Use this command to create an arbitrary number of almost real Position record
```
python manage.py seed <num>
```
You can see and change the randomness algorithm in job_recommender/management/commands/seed.py
Note: These records will be automatically indexed in ES as well.

## Searching

1- Filter term:
```
curl -X GET http://server/search/jobs/?category=Finance
curl -X GET http://server/search/jobs/?location=Tehran
curl -X GET http://server/search/jobs/?education=PhD
curl -X GET http://server/search/jobs/?gender=female
curl -X GET http://server/search/jobs/?category__term=IT&education__term=PhD

```


2- Partial match:

```
curl -X GET http://server/search/jobs/?category__contains=Finan
curl -X GET http://server/search/jobs/?title__contains=pyth
```

3- Sort
By default results are sorted by salary and _score (configured in view)
For custom sorting, you can pass the ordering GET parameter:

```
http://server/search/jobs/?category=Finance&ordering=id
```

4- Range filters:

```
http://server/search/jobs/?expired_at__gt=2021-12-09
http://server/search/jobs/?created_at__lt=2021-12-09
```

Searching null values:
```
curl -X GET http://server/search/jobs/?gender__isnull=true
```

Match search (filter would apply):
```
http://server/search/jobs/?search=category:it
http://server/search/jobs/?search=category:it&education=diploma
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)